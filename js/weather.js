(function(){
    
    console.log( 'JavaScript started.' );

    var renderer            = null;
    var scene               = null;
    var camera              = null;
    var controls            = null;
    
    var directionalLight    = null;
    
    const FOV               = 45;
    const NEAR              = 1;
    const FAR               = 1000;
    const HELPERS_ENABLED   = true;
	const SCENE_SIZE		= 200;
    
    var aspectRatio 		= 1;
	
	var latitude 			= 43.6868661;
	var longitude			= -79.7473267;
	
	/* weather */
	const MAX_REAL_WIND_SPEED = 480;
	const MAX_WIND_SPEED	= 10;
	
	var textureManager		= new THREE.LoadingManager();
	var textureLoader		= new THREE.TextureLoader( textureManager );	
	var textures = {
        dirt : {
            urls : {
                normal : 'images/textures/Rough_rock_014_NRM.jpg',
                diffuse : 'images/textures/Rough_rock_014_COLOR.jpg',
                specular : 'images/textures/Rough_rock_014_SPEC.jpg'
            },
			
        maps : {},
        
		repeat : {
                h : 2,
                v : 2
            }
        }
    }
	
	textureManager.onStart = function( url, itemsLoaded, itemsTotal ){
	}
	
	textureManager.onLoad = function(){
	}
	
	for( var textureName in textures ){
        for( var textureType in textures[ textureName ].urls ){
            textures[ textureName ].maps[ textureType ]
                = textureLoader.load( textures[ textureName ].urls[ textureType ] );
            
            if( textures[ textureName ].repeat ){
                textures[ textureName ].maps[ textureType ].wrapS = THREE.RepeatWrapping;
                textures[ textureName ].maps[ textureType ].wrapT = THREE.RepeatWrapping;
                textures[ textureName ].maps[ textureType ].repeat.set(
                    textures[ textureName ].repeat.h,
                    textures[ textureName ].repeat.v
                )
            }
        }
    }

	var collection	= {
		base : new THREE.Mesh(
			new THREE.CylinderGeometry( (SCENE_SIZE * 0.5) - 10 , 
									    (SCENE_SIZE * 0.5) - 10 , 
									    5, 
									    100, 
									    1 ),
			new THREE.MeshPhongMaterial({
                map : textures.dirt.maps.diffuse,
                normalMap : textures.dirt.maps.normal,
                specularMap : textures.dirt.maps.specular
            }) ),
		
		moon : new THREE.Mesh(
			new THREE.SphereGeometry( 12 , 
									  50 ,
									  50 ),
									    				   
			new THREE.MeshPhongMaterial({
                map : THREE.ImageUtils.loadTexture('images/textures/Moon_001_COLOR.jpg'),
                normalMap : THREE.ImageUtils.loadTexture('images/textures/Moon_001_NORM.jpg'),
                specularMap : THREE.ImageUtils.loadTexture ('images/textures/Moon_001_SPEC.jpg')
          }) ),
		
		clouds : new THREE.Group()
		
	};
	
	var cloudCount 	= 1000;
	var windSpeed 	= 5;
	var windAngle	= 0;
	var clouds = [];
	
	for( var i = 0; i < cloudCount; i++ ){
        var cloud = new THREE.Mesh(
            new THREE.BoxGeometry( 20 + ( 30 * Math.random() ), 
                                   5 + ( 5 * Math.random() ),
                                   20 + ( 30 * Math.random() ) ),
            new THREE.MeshLambertMaterial({
                color : 0xffffff,
                transparent : true,
                opacity: 0.66
            })
        );
        
        cloud.position.set( 300 * ( Math.random() - 0.5 ), 
                            90 + ( 20 * Math.random() ), 
                            200 * ( Math.random() - 0.5 ) );
        
        collection.clouds.add( cloud );
        clouds.push( cloud );
    }
	
	
	collection.base.position.y = 2.5;
	
    collection.clouds.rotation.y = windAngle;

	collection.moon.position.y = 70;
	collection.moon.position.z = -30;
	collection.moon.position.x = 20;
	
	
	var modelLoader = new THREE.ObjectLoader();
	var treeLoader = new THREE.ObjectLoader();
	
  	 /**
     * Initializes the app, after 
     * the HTML DOM has finished loading.
     *
     * @callback
     */

    function init(){
        
        console.log( 'init()' );
        
        // hides the "enable JS" message
        document.getElementById( 'no-js' ).style.display = 'none';
        
        // use a WebGL renderer if available, otherwise
        // default to canvas 2D rendering
        renderer = Detector.webgl 
                        ? new THREE.WebGLRenderer({ antialias : true, alpha : true })
                        : new THREE.CanvasRenderer();
        
        renderer.clippingPlanes = [
            new THREE.Plane( new THREE.Vector3( 1, 0, 0 ), SCENE_SIZE * 0.5 ),
            new THREE.Plane( new THREE.Vector3( -1, 0, 0 ), SCENE_SIZE * 0.5 ),
            new THREE.Plane( new THREE.Vector3( 0, 0, 1 ), SCENE_SIZE * 0.5 ),
            new THREE.Plane( new THREE.Vector3( 0, 0, -1 ), SCENE_SIZE * 0.5 )
        ];

        
        // attach the renderer's canvas tag to the body tag
        document.body.appendChild( renderer.domElement );
        
        // resize the canvas to match the browser window
        resize();
        
        // create a scene object to house everything we
        // want to render onto the canvas
        scene 
            = new THREE.Scene();
        
        // create a 3D perspective camera, from which
        // the viewpoint of the rendering is established
        camera 
            = new THREE.PerspectiveCamera( FOV, aspectRatio, NEAR, FAR );
        
        // add the camera to the scene
        scene.add( camera );
        
        // add a grid an visible axes to the scene,
        // while developing the app
        if( HELPERS_ENABLED ){
            scene.add( 
                new THREE.GridHelper( 1000, 100, 0xffff00, 0x666666 ) 
            );
            scene.add( new THREE.AxesHelper( 1000 ) );
        }
        
        // move camera 100 units away from origin in each direction
        camera.position.set( 100, 100, 100 );
        
        // spin the camera around, so it looks at origin (0,0,0)
        camera.lookAt( 0, 0, 0 );
        
        // create orbit controls and set them up
        controls = new THREE.OrbitControls( camera, renderer.domElement );
        controls.enableDamping = true;
        controls.dampingFactor = 0.25;
        controls.enableZoom    = true;

        // add an ambient light (lights up every face of every object evenly)
        scene.add( new THREE.AmbientLight( 0xffffff, 0.25 ) );
        
        // add a directional light and angle it so that there are
        // shadows and highlights in the scene
        directionalLight 
            = new THREE.DirectionalLight( 0xffffff, 0.75 );
        directionalLight.position.set( 0.25, 0.5, 0.75 );
        
        scene.add( directionalLight );
		
		for( var objectName in collection ){

			scene.add( collection[ objectName ] );

		}


		modelLoader.load( 'models/tower/castle-tower.json' , function( tower ) {
			
		scene.add( tower );
			tower.scale.set( 0.08, 0.08, 0.08 );
			tower.rotation.y = Math.PI;
			tower.position.set( -15, 6, -15 );
			
		});
		
		
		treeLoader.load( 'models/environment/tree.json' , function( tree ) {
			
		scene.add( tree );
			tree.scale.set( 0.0025, 0.0025, 0.0025 );
			tree.rotation.y = Math.PI;
			tree.position.set( 15, 6, 15 );
			
		});
	
        // enable resizing of canvas when window is resized
        window.addEventListener( 'resize', resize );
        
        // start the animation "loop"
        requestAnimationFrame( update );
		
		if( 'geolocation' in navigator ){
		   // geolocation is available
		   navigator.geolocation.getCurrentPosition( function( position ){
			   latitude 	= position.coords.latitude;
			   longitude 	= position.coords.longitude;
			     
			   gotPosition();
			   
		   } , gotPosition );
		} else {
			
			  gotPosition();
		}
    }
	
	
	/**
     * Recieves geo coordinates and makes
     * an AJAX request for weather data at those
     * coordinates.
     *
     * @callback
     */

	
	function gotPosition(){
		
		console.log( latitude, longitude );
		
		$.getJSON(
			'http://projects.hapsay.com/Web_GL_API/server.php',
			{
				
				latitude : latitude,
				longitude : longitude 
			},
			gotWeatherData
		);
	}
	
	function gotWeatherData( response ){
		
		console.log( response );
		
		if( response.status == 'success'){
			
			setWeatherConditions( {
					windSpeed: response.data.wind.speed,
					windAngle: response.data.wind.deg,
					cloudDensity: response.data.clouds.cod } );
			}
	}
	
	
	function setWeatherConditions( weatherData ){
		
		
		if( weatherData.windSpeed ){
		
		var windPercentage = windSpeed / MAX_REAL_WIND_SPEED;
		windSpeed = MAX_WIND_SPEED * windPercentage;
		   
		   
		}   
		   
		if( weatherData.windAngle ){
	
			windAngle =  weatherData.windAngle * ( Math.PI / 180 );
			collection.clouds.rotation.y = windAngle;
		   
		}  
		
		if( weatherData.cloudDensity ){
	
			var visibleCloudClount = Math.floor(( weatherData.cloudDensity / 100 ) * cloudCount );
			
			for( var i = 0; i < cloudClount; i++ ){
				var cloud = clouds[ i ];
				cloud.visible = true;
			}
			
			for( var i = 0; i < visibleCloudClount; i++ ){
				var cloud = clouds[ i ];
				cloud.visible = false;
			}
		   
		}   
		
		
	}
    
    /**
     * Updates the positions of objects in the
     * scene, and re-renders it at 60FPS if possible.
     *
     * @callback
     */
    function update(){
		
		
		for( var i = 0; i < cloudCount; i++ ){
            var cloud = clouds[ i ];
            
            cloud.position.x += windSpeed;
            
            var distanceSquared 
                = cloud.position.x * cloud.position.x + cloud.position.z * cloud.position.z;
            
            var opacity = Math.min( 1 - ( distanceSquared * 0.0002 ), 0.66 );
            
            cloud.material.opacity = opacity;
            
            if( cloud.position.x > 150 ){
                cloud.position.x = -150;
            }
        }
			
        // render the current scene, using the current camera
        renderer.render( scene, camera );
        
        // wait a bit, then run update again so that 60 FPS is the frame rate
        requestAnimationFrame( update );
    }
    
    /**
     * Resizes the render's canvas to
     * match the size of the browser window.
     *
     * @callback
     */
    function resize(){
        console.log( 'resize()' );
        
        // store height and width of browser's viewport
        var width   = window.innerWidth;
        var height  = window.innerHeight;
        
        // calculate the aspect ratio
        aspectRatio = width / height;
        
        // set the size of renderer to match viewport
        renderer.setSize( width, height );
        
        if( camera ){
            // update the aspect ratio to match new dimensions
            camera.aspect = aspectRatio;
            // use aspect ratio to re-calculate the "projection matrix"
            // this ensures scene doesn't get distorted
            camera.updateProjectionMatrix();
        }
    }
    
    // run the init function when DOM is finished loading
    window.addEventListener( 'load', init );
    
})();

