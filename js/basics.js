(function(){
    
    console.log( 'JavaScript started.' );

    var renderer            = null;
    var scene               = null;
    var camera              = null;
    var controls            = null;
	
	var directionalLight 	= null;
	
    
    const FOV               = 45;
    const NEAR              = 1;
    const FAR               = 1000;
    const HELPERS_ENABLED   = true;
    
    var aspectRatio = 1;
    
    var objects = {};
	

    
    function init(){
        console.log( 'init()' );
        
        document.getElementById( 'no-js' ).style.display = 'none';
        
        renderer = Detector.webgl 
                        ? new THREE.WebGLRenderer({ antialias : true })
                        : new THREE.CanvasRenderer();
        
        document.body.appendChild( renderer.domElement );
        
        resize();
        
        scene 
            = new THREE.Scene();
        
        camera 
            = new THREE.PerspectiveCamera( FOV, aspectRatio, NEAR, FAR );
        
        scene.add( camera );
        
        if( HELPERS_ENABLED ){
            scene.add( 
                new THREE.GridHelper( 1000, 100, 0xffff00, 0x666666 ) 
            );
            scene.add( new THREE.AxesHelper( 1000 ) );
        }
        
        camera.position.set( 100, 100, 100 );
        camera.lookAt( 0, 0, 0 );
        
        controls = new THREE.OrbitControls( camera, renderer.domElement );
        controls.enableDamping = true;
        controls.dampingFactor = 0.25;
        controls.enableZoom    = true;
        
        window.addEventListener( 'resize', resize );
        
        objects.box = { 
                        geometry : 
                            new THREE.BoxGeometry( 10, 10, 10 ),
                        material : 
                            new THREE.MeshPhongMaterial({ 
//                                color: 0xffffff,
//                                wireframe : false,
//								map : THREE.ImageUtils.loadTexture( 'images/random-textures/chesterfield-normal-map.jpg') ,
								
								color: 0Xff00ff,
								
								normalMap : THREE.ImageUtils.loadTexture( 'images/random-textures/temple-normal-map.jpg')
								
//								normalMap : THREE.ImageUtils.loadTexture( 'images/random-textures/chesterfield-normal-map.jpg') 
                            })
                      };
        
        objects.box.mesh = new THREE.Mesh( objects.box.geometry,
                                           objects.box.material );
        objects.box.mesh.position.y = 5;
		
        scene.add( objects.box.mesh );
		
		scene.add( new THREE.AmbientLight( 0Xffffff, 0.1 ) );
		
		directionalLight 
			
			= new THREE.DirectionalLight( 0Xffffff, 0.5 );
		
		directionalLight.position.set( 0, 0.25 , 1 );
		
		scene.add( directionalLight );
        
        requestAnimationFrame( update );
    }
    
    function update(){
		 
		
		objects.box.mesh.rotation.y += ( (Math.PI * 2) / 300 ) ;
		objects.box.mesh.rotation.z += ( (Math.PI * 2) / 300 ) ;
        
        renderer.render( scene, camera );
        
        requestAnimationFrame( update );
    }
    
    function resize(){
        console.log( 'resize()' );
        
        var width   = window.innerWidth;
        var height  = window.innerHeight;
        aspectRatio = width / height;
        renderer.setSize( width, height );
        
        if( camera ){
            camera.aspect = aspectRatio;
            camera.updateProjectionMatrix();
        }
    }
    
    window.addEventListener( 'load', init );
    
})();





