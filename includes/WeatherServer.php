<?php

/*
43.6868661,
-79.7473267
*/	
	
class WeatherServer{
	
	const CACHE_DIRECTORY = 'cache/';
	const CACHE_EXPIRE_TIME = 600;
	const OPEN_WEATHER_ENDPOINT =
		'http://api.openweathermap.org/data/2.5/weather?';
	
	
	private $APIKey = '';
	private $curl = null;
	
	
	public function __construct( $APIKey ){
		
//		echo 'WeatherServer->__construct<br />';
		$this->APIKey = $APIKey;
		
		$this->curl = curl_init();
		
		$options = array(
		
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HEADER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_SSL_VERIFYPEER => false
//		CURLOPT_URL => self::OPEN_WEATHER_ENDPOINT
		
		);
		
		curl_setopt_array( $this->curl, $options );
		
	}
	
	public function getCurrentWeather(  $latitude, $longitude ){
		
//		echo 'WeatherServer->getCurrentWeather()<br />';
//		echo "lat: $latitude, lng: $longitude";
		
		$jsend = array();
		
		if( is_numeric( $latitude ) ){
			
			if( is_numeric( $longitude )) {
				
				
				$filepath = self::CACHE_DIRECTORY . "{$latitude}X{$longitude}.json";
				
				if( file_exists( $filepath ) and ( (time() - filemtime( $filepath ) ) < self::CACHE_EXPIRE_TIME )
				  
				  ){
					
				  	$jSend[ 'status' ] = 'success';
					$jSend[ 'data' ] = json_decode( file_get_contents( $filepath ) );
					$jSend[ 'data' ]->cachedVersion = true;
					
				} else {
				

					$requestUrl = self::OPEN_WEATHER_ENDPOINT
						."lat=$latitude&lon=$longitude&"
						. "APPID=" . $this->APIKey;

					curl_setopt( $this->curl, CURLOPT_URL, $requestUrl );

					$response = curl_exec( $this->curl );

					$httpStatus = curl_getinfo( $this->curl, CURLINFO_HTTP_CODE );

					$jSend = array();

					if( $httpStatus == 200 ){

						$responseObject = json_decode( $response );

						if( $responseObject->cod == 200 ){

							$jSend[ 'status' ] = 'success';
							$jSend[ 'data' ] = $responseObject;
							
							file_put_contents( $filepath, json_encode( $responseObject ) );

						} else {

							$jSend[ 'status' ] = 'fail';
							$jSend[ 'data' ] = $responseObject;
						}	


					} else {

						$jSend[ 'status' ] = 'error';
						$jSend[ 'message' ] = "There was an error connecting to the OpenWeatherMap API server, status code $httpStatus";

					}	
				}
				
			} else {
				
				$jSend [ 'status' ] = 'fail';
				$jSend [ 'data' ] = array( 'longitude' => 'The longitude is not a valid number' );
				
			} 
					
		} else {	
			
			$jSend [ 'status' ] = 'fail';
			$jSend [ 'data' ] = array( 'latitude' => 'The latitude is not a valid number' );
			
			
		}
		
		header( 'Access-Control-Allow-Origin: *' );
		header( 'Content-Type: application/json ' );
		echo json_encode( $jSend, JSON_PRETTY_PRINT );
		
	}
	
	
	public function __destruct(){
		
		
//		echo 'WeatherServer->__destruct<br />';
//	
		curl_close( $this->curl );
		
		
	}

}